<?php
	
	$this->load_template('home_header.php');
        $this->load_langfile('outside/signin.php');
        $this->load_langfile('outside/home.php');
        $this->load_langfile('outside/signup.php');
	
?>
<div class="homepage_maindiv">
    <div align="left" class="homepage_innerdiv">
        <div class="intro-text" style="height:180px;">
            <span style=" direction:rtl;">به شبکه اجتماعی ما خوش آمدید!</span><br />
            <?= $D->intro_txt ?>
        </div>
        <div id="registration" style="">
            <form method="post" action="<?= $C->SITE_URL . 'signup' ?>">
                <div style="margin: 4px 4px 12px 12px; direction:rtl;font:8pt tahoma; float:right;"><?= $this->lang('signin_reg_title') . " " .  $this->lang('os_welcome_btn') ?>!</div>
                <?php if(!$C->USERS_EMAIL_CONFIRMATION) { ?>
                <input autocomplete="off" id="fullname" style="float:right;font:8pt tahoma;" placeholder="<?= $this->lang('signup_step2_form_fullname') ?>" name="fullname" size="20" tabindex="5" type="text" />
                <?php } ?>
                <input autocomplete="off" id="email" style="float:right;font:8pt tahoma;" placeholder="<?= $this->lang('signup_step2_form_email') ?>" name="email" size="20" tabindex="6" type="text" />
                <?php if(!$C->USERS_EMAIL_CONFIRMATION) { ?>
                <input autocomplete="off" id="password" style="float:right;font:8pt tahoma;" placeholder="<?= $this->lang('signup_step2_form_password') ?>" name="password" size="20" tabindex="7" type="password" />
                <?php } ?>
                <br><br><br>
     <ul class="uibutton-group" style="float:right; margin-right:6px;">
    <li><a class="uibutton" href="signin/forgotten">رمز عبورم را فراموش کردم!</a></li>
    <li><a class="uibutton" href="signin">ورود</a></li>
    <li><a href="#"><button class="uibutton" type="submit">عضویت</button></a></li>
</ul>
            </form>
        </div>
        <div class="klear"></div>
    </div>
</div>

</div>

<div class="golden_line"></div>
<?php
	
	$this->load_template('home_footer.php');
	
?>