<?php
	
	$this->load_langfile('outside/footer.php');
	$this->load_langfile('inside/footer.php');
	
?>
<style>
a { text-decoration: none; }
a:hover { text-decoration: none; }
</style>
<div id="subfooter" style="width:980px; margin:0 auto;font:8pt tahoma;">
				<div id="sfleft" style="font:8pt tahoma;">
					شبکه اجتماعی داستـــان زندگی
					&middot;
					<a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>contacts"><?= $this->lang('ftr_contacts') ?></a>
				</div>
                	<div id="sfright" style="font:8pt tahoma;line-height:22px;">
					<a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">شرایط استفاده</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">حریم خصوصی</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">ایده و توسعه</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">کلید انتشار</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">پالس</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">واسط برنامه نویسی</a>&middot;
                    <a style="font:8pt tahoma;" href="<?= $C->SITE_URL ?>">تبلیغات</a>
                    <br />
                    سازگار با: Firefox 3+ ، Chrome و +Opera 12
				</div>
			</div>
		</div>
		
		<?php
			// Important - do not remove this:
			$this->load_template('footer_cronsimulator.php');
			if( $C->DEBUG_MODE ) { $this->load_template('footer_debuginfo.php'); }
		?>
		
		<?php
			@include( $C->INCPATH.'../themes/include_in_footer.php' );
		?>
		
	</body>
</html>